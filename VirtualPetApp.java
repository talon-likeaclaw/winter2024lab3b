public class VirtualPetApp {
	
	public static void main(String[] args) {
		java.util.Scanner reader = new java.util.Scanner(System.in);
		int ARRAY_SIZE = 4;
		
		// Intialize array of bird instances from user input
		Bird[] flockOfBirds = new Bird[ARRAY_SIZE];
		for (int i = 0; i < flockOfBirds.length; i++) {
			flockOfBirds[i] = new Bird();
			System.out.println("Enter a bird type (Eagle, Owl, Crow, Flamingo):");
			flockOfBirds[i].type = reader.nextLine();
			System.out.println("Enter the size of the bird (Small, Medium, Large):");
			flockOfBirds[i].size = reader.nextLine();
			System.out.println("Can this type of bird fly? (true/false)");
			flockOfBirds[i].canFly = Boolean.parseBoolean(reader.nextLine());
		}
		
		System.out.println("Last bird type: " + flockOfBirds[flockOfBirds.length - 1].type);
		System.out.println("Last bird size: " + flockOfBirds[flockOfBirds.length - 1].size);
		System.out.println("Last bird can fly?: " + flockOfBirds[flockOfBirds.length - 1].canFly);
		
		flockOfBirds[0].fly();
		flockOfBirds[0].sing();
		
	}
	
}