public class Bird {
	// Fields
	public String type;
	public String size;
	public boolean canFly;
	
	
	// Allows to bird to fly, unless the bird is unable..
	public void fly() {
		if (this.canFly) {
			System.out.println("Flap flap flap sooooooooar, flap flap soar..");
		} else {
			System.out.println("The " + this.type + " flaps it's wings, but nothings happens..");
		}
	}
	
	// Allows the bird to sing depending on the type
	public void sing() {
		if (this.type.equals("Eagle")) {
			System.out.println("Scraaaaaaw!!!");
		} else if (this.type.equals("Owl")) {
			System.out.println("Hooot hooot!");
		} else if (this.type.equals("Crow")) {
			System.out.println("Caaaaw caaaw!");
		} else {
			System.out.println("Honk grunt growl!");
		}
		
	}
	
}